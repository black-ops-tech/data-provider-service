package com.dataproviderservice.DataProvider.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dataproviderservice.DataProvider.entity.DataProvider;
import com.dataproviderservice.DataProvider.services.DataProviderService;

@RestController
@RequestMapping("/data/provider")
public class DataProviderController {

	
	@Autowired
	DataProviderService dataService;
	
	@GetMapping("/message")
	public String getMessage() {
		return "Service Created";
	}
	
	
	@GetMapping("/fetchAll")
	public List<DataProvider> getAllUserData(){
		return dataService.getAllData(); 
	}
	
	
	
}
