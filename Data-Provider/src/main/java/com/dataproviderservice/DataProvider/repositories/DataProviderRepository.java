package com.dataproviderservice.DataProvider.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.dataproviderservice.DataProvider.entity.DataProvider;

@Repository
public interface DataProviderRepository extends JpaRepository<DataProvider, Long> {}