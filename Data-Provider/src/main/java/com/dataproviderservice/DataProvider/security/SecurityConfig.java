package com.dataproviderservice.DataProvider.security;
import java.net.http.HttpClient;
import org.apache.catalina.core.ApplicationFilterChain;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Configuration
public class SecurityConfig {
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().authorizeHttpRequests()
		.requestMatchers("/data/provider/message").permitAll()
		.requestMatchers("/data/provider/fetchAll").permitAll()
				.anyRequest().authenticated().and().formLogin();
		return httpSecurity.build();
	}
}
