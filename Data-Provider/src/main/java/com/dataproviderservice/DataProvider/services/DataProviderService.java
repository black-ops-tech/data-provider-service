package com.dataproviderservice.DataProvider.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataproviderservice.DataProvider.entity.DataProvider;
import com.dataproviderservice.DataProvider.repositories.DataProviderRepository;

@Service
public class DataProviderService {
	
	@Autowired
	DataProviderRepository dataProviderRepository;
	
	public List<DataProvider> getAllData() {
		return dataProviderRepository.findAll();
	}
	
	
}