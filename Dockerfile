FROM openjdk:17
EXPOSE 8080
ADD target/Data-Provider-0.0.1-SNAPSHOT.jar Data-Provider-0.0.1-SNAPSHOT.jar
ENTRYPOINT [ "java","-jar","/Data-Provider-0.0.1-SNAPSHOT.jar" ]
